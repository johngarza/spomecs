// Load the application once the DOM is ready, using `jQuery.ready`:
$(function(){
	//instantiate our model
	window.ReportParams = new SParams();
	window.Config = new Config();
	window.UserInfo = new UserInfo();
	window.DataSet = new SPOMECSDataList();
	var uri = window.Config.get("base_uri") + window.Config.get("user_get") ;
	$.ajax({
		url: uri,
		method: "GET",
		dataType: "json",
		error: function() {
			//do nothing?
			//alert user that they need to log in!
		},
		success: function(user_data) {
			var contact_info = user_data["contacts"]["contact"];
			if (contact_info) {
				window.UserInfo.set({
					username: contact_info["r25user"]["r25_username"],
					name: contact_info["contact_name"],
					user_id: contact_info["contact_id"]
				});
			}
		},
		complete: function() {
			//cleanup
			//render dataset
			//remove previously existing dataset view
				//disable spinner, hide modal dialog
			window.app = new SPOMECSRouter();
			Backbone.history.start();	
		}
	});
});


var normalize_orgs = function(r25_obj) {
	var newItem = { label: r25_obj.organization_title._text, value: r25_obj.organization_id._text }
	return newItem;
}

var time_print = function(date_str) {
	var dt = moment(date_str);
	return dt.format(TM_FORMAT);
};

var date_print = function(date_str) {
	var dt = moment(date_str);
	return dt.format(DT_FORMAT_FULL);
}

var normalize_contacts = function(r25_obj) {
	var newItem = {
		label: r25_obj.contact_name,
		value: r25_obj.contact_id
	}
	return newItem;
}

var xml2Str = function(xmlNode) {
   try {
      // Gecko- and Webkit-based browsers (Firefox, Chrome), Opera.
      return (new XMLSerializer()).serializeToString(xmlNode);
  } catch (e) {
     try {
        // Internet Explorer.
        return xmlNode.xml;
     } catch (e) {  
        //Other browsers without XML Serializer
     }
   }
   return false;
}

var parse_xml = function(text) {
	var doc;
	if (window.DOMParser) {
		var parser = new DOMParser();
		doc = parser.parseFromString(text, "text/xml");
	} else if (window.ActiveXObject) {
		doc = new ActiveXObject("Microsoft.XMLDOM");
		dom.async = "false";
		dom.loadXML(text);
	} else {
		throw new Error("Cannot parse XML");
	}
	return doc;
}
/*
if(!Array.isArray) {
  Array.isArray = function (arg) {
    return Object.prototype.toString.call(arg) == '[object Array]';
  };
}
*/
