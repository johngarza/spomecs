$(function(){
	window.NavDisplayView = Backbone.View.extend({
		template: _.template($('#NavDisplayView').html()),
		_st_dt : moment(),
		_nx_dt : moment(),
		_pv_dt : moment(),
		events: {
		},
		initialize: function() {
			_.bindAll(this, "render");
			this.model.bind('change', this.render);
		},
		render: function() {
			this._st_dt = moment(this.model.get("start_dt"));
			this._nx_dt = moment(this._st_dt).add('days', 1);
			this._pv_dt = moment(this._st_dt).subtract('days', 1);
			var navModel = {
				sp_query: this.model.get("space_query_id"),
        st_dt_pretty: this._st_dt.format(DT_FORMAT_FULL),
				st_dt : this._st_dt.format(DT_FORMAT),
				nx_dt : this._nx_dt.format(DT_FORMAT),
				pv_dt : this._pv_dt.format(DT_FORMAT)
			};
			$(this.el).html(this.template(navModel));
		}
	});
});