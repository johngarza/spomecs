$(function(){
	<!-- shameless copy from http://liquidmedia.ca/blog/2011/02/backbone-js-part-3/ -->
	window.UpdatingItemView = Backbone.View.extend({
		tagName : "tbody",
		showHeader: false,
		template: _.template($("#ItemView").html()),
		initialize: function() {
			this.render = _.bind(this.render, this);
			this.model.bind('change', this.render);
		},
		setShowHeader: function(show) {
			this.showHeader = show;
		},
		render: function() {
			var itemModel = this.model.toJSON();
			itemModel.showHeader = this.showHeader;
			$(this.el).html(this.template(itemModel));
			return this;
		}
	});
});