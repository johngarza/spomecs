$(function(){
	<!-- shameless copy from http://liquidmedia.ca/blog/2011/02/backbone-js-part-3/ -->
	window.UpdatingCollectionView = Backbone.View.extend({
		_current_dt : null,
		_childViews : null,
		initialize : function(options) {
			var updatingCollectionViewThis = this;
			_(this).bindAll('add', 'remove');
			if (!options.childViewConstructor) throw "no child view constructor provided";
			this._childViewConstructor = options.childViewConstructor;
			this._childViews = [];
			this.collection.each(function(model) {
				updatingCollectionViewThis.add(model);
				this.add;
			});
			this.collection.bind('add', this.add);
			this.collection.bind('remove', this.remove);
		},
		add : function(model) {
			var childView = new this._childViewConstructor({
				model : model
			});
			this._childViews.push(childView);
			if (this._rendered) {
				$(this.el).append(childView.render().el);
			}
		},
		remove : function(model) {
			var viewToRemove = _(this._childViews).select(function(cv) { return cv.model === model; })[0];
			this._childViews = _(this._childViews).without(viewToRemove);
			if (this._rendered) $(viewToRemove.el).remove();
		},
		render : function() {
			var report_st = window.ReportParams.get("start_dt");
			var updatingCollectionViewThis = this;
			this._rendered = true;
			$(this.el).empty();
			_(this._childViews).each(function(childView) {
				var current_dt = childView.model.get("reservation_start_dt")._text;
				if (date_print(current_dt) != date_print(report_st)) {
					report_st = current_dt;
					childView.setShowHeader(true);
				}
				$(updatingCollectionViewThis.el).append(childView.render().el);
			});
			return this;
		}
	});
});