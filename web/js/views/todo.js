$(function(){
  window.TodoView = Backbone.View.extend({
    template: _.template($('#TodoModal').html()),
    _modal: null,
    _contacts: null,
    _is_on: false,
    events: {
      "click #saveTodo" : "saveTodo",
      "change #due_dt" : "updateField",
      "change #comment" : "updateField"
    },
    initialize: function() {
      this.model.set({assigned_by_id: window.UserInfo.get("user_id"), assigned_by: window.UserInfo.get("user_id")});
    },
    updateTypeAhead: function() {
      var todoViewThis = this;
      var typeAheadSorter = function (items) {
        var beginsWith = [];
        var caseSensitive = [];
        var caseInsensitive = [];
        var item = null;
        while (item = items.shift()) {
          if (!item.label.toLowerCase().indexOf(this.query.toLowerCase())) {
            beginsWith.push(item);
          } else {
            if (~item.label.indexOf(this.query)) {
              caseSensitive.push(item);
            } else { 
              caseInsensitive.push(item);
            }
          }
        }
          return beginsWith.concat(caseSensitive, caseInsensitive)
      };
      var typeAheadMatcher = function(item) {
        var result =  ~item.label.toLowerCase().indexOf(this.query.toLowerCase());								
        return result;
      };
      $('.typeahead').typeahead({
        source: todoViewThis._contacts,
        items: 10,
        minLength: 3,
        matcher: typeAheadMatcher,
        sorter: typeAheadSorter,
        select:  function () {
          var selected = this.$menu.find('.active');
          var updtObj = {};
          var val = selected.attr('data-value');
          var label = selected.attr('label');
          var name = selected.attr('data');
          var id = this.$element.attr("id");
          var name_id = "#" + id;
          var id_id = name_id + "_id";
  
          updtObj[id] = val;
          todoViewThis.model.set(updtObj);
          $(name_id).val(label);
          $(id_id).val(val);
          this.$element.val(this.updater(label)).change();
          //todoViewThis.enableNext();
          return this.hide();
        },
        render: function (items) {
          var todoViewThis = this;
          items = $(items).map(function (i, item, idx) {
            i = $(todoViewThis.options.item).attr('data-value', item.value).attr('data', item.data).attr('label', item.label);
            //i = $(todoViewThis.options.item).attr('data', item.data);
            i.find('a').html(todoViewThis.highlighter(item.label));
            return i[0];
          });
          items.first().addClass('active');
          this.$menu.html(items);
          return this;
        },
        updater: function(item) { return item;}
      });
    },
    render : function() {
      var todoViewThis = this;
      var config = app._app_config;
      var uri = config.get("base_uri") + config.get("cons_get") + config.get("org_id");
      
      var successHandler = function(data) {
        cons_data = data["contacts"]["contact"];
        //todoViewThis_contacts = data;
        todoViewThis._contacts = _.map(cons_data, normalize_contacts);
        //todoViewThis._contacts = cons_data.map(normalize_contacts);
        todoViewThis.postRender();
      }
      $.ajax({
        url: uri,
        async: false,
        method: "GET",
        dataType: "json",
        error: function() {
          $.ajax({ url: "contacts.html", async: false, method: "GET", dataType:"json", success: successHandler});
        },
        success: successHandler
      });
    },
    postRender: function() {
      var todoViewThis = this;
      $(this.el).html(this.template(this.model.toJSON()));
      $("#due_dt").datepicker({
        inline: false,
        minDate: new Date(),
        dateFormat: UI_DT_FORMAT
      });
      this.updateTypeAhead();
    },
    show: function() {
      $("#TodoForm").validate({
        errorElement: "span",
        errorPlacement: function(errorObj, eleObj) {
          errorObj.appendTo(eleObj.closest(".input"));
        },
        highlight: function(element, errorClass) {
          $(element).closest(".input").addClass(errorClass).addClass("alert").addClass("alert-error");
        },
        unhighlight: function(element, errorClass) {
          $(element).closest(".input").removeClass(errorClass).removeClass("alert").removeClass("alert-error");
        }
      });
    },
    enableNext: function() {
      var vCheck = $("#TodoForm").valid();
      //console.log(vCheck);
      if (vCheck) {
        this.enableButtons();
      } else {
        this.disableButtons();
      }
    },
    disableButtons: function() {
      $(".btn").addClass("disabled");
      $(".btn").attr("disabled", "disabled");
    },
    enableButtons:function() {
      $(".btn").removeAttr("disabled");
      $(".btn").removeClass("disabled");
    },
    saveTodo: function() {
      var todoViewThis = this,
      todo_id = "-1",
      config = app._app_config,
      post_uri = config.get("base_uri") + config.get("todo_post"),
      put_uri = config.get("base_uri") + config.get("todo_put");
      var completeTodoCreate = function() {
        var target_id = "#" + todoViewThis.model.get("rsrv_id");
        $(target_id).trigger("click");
      }
      
      var handlePostResponse = function(post_resp_txt) {
        var post_resp = parse_xml(post_resp_txt);
        //var post_resp = $.parseXML(post_resp_txt);
        //we must update the following fields in the todo xml POST content:
        // -type_id - numeric (1 for private, 2 for public)
        // -priority_id - numeric (1 for highest priority)
        //-object_type - numeric (1 for events)
        //-object_id - numeric (event id if using 1 for object_type)
        //-read - boolean 'T' or 'F' (has the todo been read, should default to 'F')
        //-cur_state_id - numeric (1 for In Progress)
        //-name - string (name of the todo)
        //-due_date - Date String (in the format of YYYYMMDD)
        //-assigned_to_id - numeric (R25 user contact_id)
        //-assigned_by_id - numeric (R25 user contact_id)
        //-comment - string - the text of the todo
				
        //we need to know the todo_id for the PUT uri
        var todo_id_node = $(post_resp).find("r25\\:todo_id, todo_id")[0];
        var todo_id = $(todo_id_node).text();
        //fill in the data from our model
        $(post_resp).find("r25\\:type_id, type_id").text(todoViewThis.model.get("type_id"));
        $(post_resp).find("r25\\:priority_id, priority_id").text(todoViewThis.model.get("priority_id"));
        $(post_resp).find("r25\\:object_type, object_type").text(todoViewThis.model.get("object_type"));
        $(post_resp).find("r25\\:object_id, object_id").text(todoViewThis.model.get("event_id"));
        $(post_resp).find("r25\\:read, read").text(todoViewThis.model.get("read"));
        $(post_resp).find("r25\\:cur_state_id, cur_state_id").text(todoViewThis.model.get("cur_state_id"));
        $(post_resp).find("r25\\:name, name").text(todoViewThis.model.get("name"));
        $(post_resp).find("r25\\:due_date, due_date").text(todoViewThis.model.get("due_dt"));
        $(post_resp).find("r25\\:assigned_to_id, assigned_to_id").text(todoViewThis.model.get("assigned_to"));
        $(post_resp).find("r25\\:assigned_by_id, assigned_by_id").text(todoViewThis.model.get("assigned_by"));
        $(post_resp).find("r25\\:comment, comment").text(todoViewThis.model.get("comment"));
        //our new todo.xml ready for POST'ing
        //var post_data = xml2Str(post_resp);
        var post_data = (new XMLSerializer()).serializeToString(post_resp);
        $.ajax({
          url: put_uri + "todo_id=" + todo_id,
          method: "PUT",
          dataType: "text",
          contentType: "text/xml",
          data: post_data,
          error: function() {
            console.log("ERRRRRRRROR while PUT'ing new todo document");
          },
          success:function(put_resp) {
            console.log("Successfully created todo:");
            console.log(put_resp.toString());
          },
          complete: completeTodoCreate
        });
      }
      
      $.ajax({
        url: post_uri,
        method: "POST",
        dataType: "text",
        accepts: "text/xml",
        error: function() {
          console.log("Error while getting new todo template");
        },
        success: function(post_resp_txt) {
          handlePostResponse(post_resp_txt);
        }
      });
      //we're going to have to way for the two ajax call to complete
      //let's disable our buttons and activate a spinner to let the user know
     // this.disableButtons();
      //$(".activity").spin("small");
    },
    updateField: function(e) {
      var field = $(e.currentTarget),
      val = field.val(),
      e_id = field.attr('id')+"",
      updtObj = {};
      updtObj[e_id] = val;
      this.model.set(updtObj);
      //this.enableNext();
    },
  });
});
