$(function(){
	window.SetupView = Backbone.View.extend({
		template: _.template($('#SetupModal').html()),
		_modal: null,
		_is_on: false,
		events: {
			"click #generate" : "generate",
			"change select#space_query_id" : "updateSpaceQuery",
			"change #start_dt" : "updateDate",
			"change #end_dt" : "updateDate",
			"click #closeSetup" : "closeSetup"
		},
		initialize: function() {

		},
		render: function() {
			$(this.el).html(this.template(this.model.toJSON()));
			$("#start_dt").datepicker({
				inline: false,
				minDate: new Date(),
				dateFormat: UI_DT_FORMAT
			});
			$("#end_dt").datepicker({
				inline: false,
				minDate: new Date(),
				dateFormat: UI_DT_FORMAT
			});
			var setupViewThis = this;
			this._modal = $("#Setup").modal({show: false});
			this._modal.on('hidden', function () {
				setupViewThis._is_on = false;
			});
			this._modal.on('show', function () {
				setupViewThis._is_on = true;
			});
		},
		show: function() {
			if (this._modal && !this._is_on) {
				this.enableButtons();
				this._modal.modal("show");
			}
		},
		hide: function() {
			if (this._modal && this._is_on) {
				this._modal.modal("hide");
			}
		},
		disableButtons: function() {
			$(".btn").addClass("disabled");
			$(".btn").attr("disabled", "disabled");
		},
		enableButtons:function() {
			$(".btn").removeAttr("disabled");
			$(".btn").removeClass("disabled");
		},
		generate: function() {
			this.disableButtons();
			var path = "generate/" + this.model.get("space_query_id") + "/" + this.model.get("start_dt");
			app.navigate(path, true);
		},
		closeSetup: function() {
			//do nothing else but close the modal dialog
			if (this._modal) {
				this._modal.modal("hide");
			}
			app.navigate("index", true);
		},
		updateSpaceQuery: function(e, t) {
			var field = $(e.currentTarget),
				val = $("option:selected", field).val(),
				e_id = field.attr('id');
			this.model.set({space_query_id : val});
//			this.model.save();
		},
		updateDate: function(e) {
			var field = $(e.currentTarget),
				val = $(field).val(),
				e_id = field.attr('id')+"",
				updtObj = {};
			updtObj[e_id] = val;
			this.model.set(updtObj);
		}
	});
});