$(function(){
	window.DataSetView = Backbone.View.extend({
		el: $("#content"),
		template: _.template($("#CollectionView").html()),
		_collectionView: null,
		_todo_view: null,
		_todo_model: null,
		events : {
				"click .reload": "reloadData",
				"click .row_collapse": "collapseData",
				"click .addTodo": "addTodo"
		},
		addTodo: function(e) {
			var rid = $(e.target).attr("rsrv_id");
			var eid = $(e.target).attr("event_id");
      var form_id = '#todo-form-container-' + rid + '-' + eid;
			var data = this.collection.get(rid),
			st_dt = moment(data.get("reservation_start_dt")._text, DT_FORMAT),
			dataSetViewThis = this;
      var todoModel = new TodoParams();
      
			//this._todo_model = new TodoParams();
			todoModel.set({
				event_id: eid,
				rsrv_id: rid,
				due_dt: st_dt.format(DT_FORMAT),
				start_dt: st_dt.format(DT_FORMAT)
			});
      var todoView = new TodoView({el: $(form_id), model: todoModel });
      todoView.render();
/*      
			if (this._todo_view) { this._todo_view.destroy(); }
			this._todo_view = new TodoView({el: $(form_id), model: this._todo_model});
			this._todo_view.render();
			//this._todo_view.show();
      */
		},
		collapseData: function(e) {
			var id = $(e.target).attr("id");
			var rows = $('.' + id).toggle("slow");
		},
		reloadData: function(e) {
			var id = $(e.target).attr("id"),
				data = this.collection.get(id);
				st_dt = moment(data.get("reservation_start_dt")._text, DT_FORMAT);
			this.reload(id, st_dt);
		},
		reload: function(rsrv_id, st_dt) {
			var dataSetViewThis = this,
				start_dt = moment(st_dt, DT_FORMAT).format(DT_FORMAT),
				end_dt = moment(st_dt, DT_FORMAT).format(DT_FORMAT),
				config = app._app_config,
				uri = config.get("base_uri") + config.get("rsrv_get");
			uri = uri + "rsrv_id=" + rsrv_id;
			uri = uri + "&start_dt=" + start_dt + "&end_dt=" + end_dt + "&cache=none";
			$(".activity").spin("small");
			$.ajax({
				url: uri,
				method: "GET",
				dataType: "json",
				error: function() {	},
				success: function(rsrv_resp) {
					var rsrv_data = [];
					if (rsrv_resp["reservations"]) {
						rsrv_data.push(rsrv_resp["reservations"]["reservation"]);
					}
					if (_.isArray(rsrv_data)) {
						$(rsrv_data).each(function(idx){
							this.id = this.reservation_id._text;
							dataSetViewThis.collection.add(this, {merge:true});
						});
					}
				},
				complete: function() {
					$(".activity").spin(false);
				}
			});
		},
		initialize: function() {
			this._collectionView = new UpdatingCollectionView({
				collection : this.collection,
				childViewConstructor : UpdatingItemView
			});
		},
		setupGo: function() {
			app.navigate("setup", true);
		},
		render: function() {
			$(this.el).empty();
			var st_dt = "";
			var jsonModel = this.collection.toJSON();
			if (this.collection.models.length > 0) {
				st_dt_str = this.collection.models[0].get("reservation_start_dt")._text;
				st_dt = moment(st_dt_str, R25_DT_FORMAT);
				jsonModel.dt = st_dt.format(DT_FORMAT_FULL);
			} else {
				jsonModel.dt = "";
			}

			$(this.el).html(this.template(jsonModel));
			this._collectionView.el = this.$("#items");
			this._collectionView.render();
		},
		destroy: function() {
			var dataSetViewThis = this;
			dataSetViewThis.undelegateEvents();
			if (dataSetViewThis.el.empty) {
			this.el.fadeOut("fast", function(){
				dataSetViewThis.el.empty();
			});
			}	
		}
	});
});