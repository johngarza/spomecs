$(function(){
	<!-- shameless copy from http://liquidmedia.ca/blog/2011/02/backbone-js-part-3/ -->
	window.UpdatingHeaderView = Backbone.View.extend({
		tagName : "tbody",
		template: _.template($("#HeaderView").html()),
		initialize: function() {
			this.render = _.bind(this.render, this);
			this.model.bind("change", this.render);
		},
		render: function() {
			$(this.el).html(this.template(this.model.toJSON()));
			return this;
		}
	});
});