/* --- Date Format Constants --- */
var DT_FORMAT = 'YYYY-MM-DD';
var DT_FORMAT_FULL = 'dddd, MMMM Do YYYY';
var DT_REQUEST_FORMAT = 'YYYYMMDD';
var TM_FORMAT = 'h:mma';
var UI_DT_FORMAT = 'yy-mm-dd';  //NOT A moment.js FORMAT, this is a jquery ui date format
var R25_DT_FORMAT = 'YYYY-MM-DDTHH:mm:ssZZ';

/* ---- MODELS ----*/
window.Config = Backbone.Model.extend({
	defaults: {
		base_uri : "/r25ws/wrd/run/",
		rsrvs_get : "reservations.xml?lint=T&scope=extended&include=event+spaces+resources+customers+text+workflow+attributes&otransform=spomecs/add_todos.xsl+json.xsl&",
		rsrv_get :"reservation.xml?lint=T&scope=extended&include=event+spaces+resources+customers+text+workflow+attributes&otransform=spomecs/add_todos.xsl+json.xsl&",
		cons_get : "contacts.xml?scope=simple&otransform=json.xsl&lint=T&simple=T&",
		user_get: "contact.xml?scope=simple&otransform=json.xsl&lint=T&simple=T&current=T",
		org_id : "organization_id=59",
		todo_post : "todo.xml",
		todo_put : "todo.xml?"
	}
});

window.UserInfo = Backbone.Model.extend({
	defaults: {
		username: "uctr",
		name: "name",
		user_id: "1",
		email: "emcsevents@utsa.edu"
	}
});

window.SParams = Backbone.Model.extend({
	defaults: {
		space_query_id: 211911,
		start_dt: moment().format(DT_FORMAT),
		end_dt: moment().format(DT_FORMAT),
		state: null
	}
//	lawnchair: new Lawnchair({ name: "params"}, function(store){}) // Unique name within your app.
});

window.RsrvData = Backbone.Model.extend({
	defaults: {
		resource_reservation: null,
		space_reservation: null,
		todos: null
	}
});

window.TodoParams = Backbone.Model.extend({
	defaults: {
		type_id: 2, //Public
		priority_id: 1, //highest priority
		object_type: 1,  //event todo
		read: 'F',
		cur_state_id: 1, //in-progress
		name: "SPOMECS Review - " + moment().format(DT_FORMAT),
		due_dt: moment().format(DT_FORMAT),
		assigned_to: null,
		assigned_by: null,
		rsrv_id: -1,
		event_id: -1,
		comment: "this is a fake todo",
		start_dt: moment().format(DT_FORMAT)
	}
});

window.HeaderData = Backbone.Model.extend({
	defaults: {
		day_dt: (new Date()).valueOf()
	},
	initialize: function() {}
});

window.SPOMECSDataList = Backbone.Collection.extend({
	model: RsrvData,
	comparator: function(item) {
		var item_dt = new Date(item.get("reservation_start_dt")._text)
		return item_dt.valueOf();
	}
});


