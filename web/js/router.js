/* ---- ROUTER/CONTROLLER ---- */
var SPOMECSRouter = Backbone.Router.extend({
	_report_params: null,
	_app_config: null,
	_todo_params: null,
	_data_set: null,
	_dataset_view: null,
	_setup_view: null,
	_nav_view: null,
	_inspect_view: null,
	_has_generated: null,
	_view_forward: "",  //used for setting forward app.navigate rule after successful authentication
	
	routes: {
		"reset": "reset",
		"": "index",
		"setup": "setup",
		"generateData": "generateData",
		"generate/:space_query/:st_dt": "generate",
		//"inspect/:rsrv_id": "inspect",
		"reload/:rsrv_id/:start_dt": "reload"
	},
	/*
	reload: function(rsrv_id, st_dt) {
		var routerThis = this,
			start_dt = moment(st_dt, DT_FORMAT).format(DT_REQUEST_FORMAT),
			end_dt = moment(st_dt, DT_FORMAT).format(DT_REQUEST_FORMAT),
			config = this._app_config,
			uri = config.get("base_uri") + config.get("rsrv_get") + "reservations_id=" + rsrv_id;

		uri = uri + "&start_dt=" + start_dt + "&end_dt=" + end_dt + "&cache=none";
		$(".activity").spin("small");
		$.ajax({
			url: uri,
			method: "GET",
			dataType: "json",
			error: function() {	},
			success: function(rsrv_data) {
				if (Array.isArray(rsrv_data)) {
					$(rsrv_data).each(function(idx){
						this.id = this.reservation_id._text;
						routerThis._data_set.add(this, {merge:true});
					});
				}
			},
			complete: function() {
				$(".activity").spin(false);
			}
		});
	},
	*/
	initialize: function(options) {
		this._has_generated = false;
		this._report_params = ReportParams;
		this._app_config = Config;
		this._data_set = DataSet;
		this._setup_view = new SetupView({el: $("#setup-modal-container"), model: this._report_params});
		this._nav_view = new NavDisplayView({el: $("#nav-display"), model: this._report_params});
		this._setup_view.render();
		this._nav_view.render();

		if (!this._dataset_view) {
			this._dataset_view = new DataSetView({
				collection : this._data_set
			});
		}
		this._dataset_view.render();
	},
	index: function() {
		console.log("route: index");
    this.generateData();
    //app.navigate("#generate");
    /*
		if (!this._has_generated) {
			app.navigate("#setup", true);
		}
		this._dataset_view.render();
    */
	},
  /*
	inspect: function(rsrv_id) {
		var rsrv_model = this._data_set.get(rsrv_id);
	},
*/
	setup: function() {
		console.log("route: setup");
		this._setup_view.show();
		//this._dialog_view.render();
	},
	
	generate: function(space_query_id, st_dt) {
		var newDt = moment(st_dt, DT_FORMAT);
		this._report_params.set({space_query_id: space_query_id, start_dt: newDt.format(DT_FORMAT) });
		this.generateData();
		//start spinner
	},
	generateData: function() {
		var routerThis = this,
			space_query_id = this._report_params.get("space_query_id"),
			start_dt = moment(this._report_params.get("start_dt"), DT_FORMAT).format(DT_REQUEST_FORMAT),
			config = this._app_config,
			uri = config.get("base_uri") + config.get("rsrvs_get") ;

		uri = uri + "space_query_id=" + space_query_id;
		uri = uri + "&start_dt=" + start_dt + "&end_dt=" + start_dt;
    // + "&cache=1";
		this._has_generated = true;
		$(".activity").spin("small");
    $('#content').hide('slow');
		//do something with ReportParams
		//format of request:
		// /spomecs/cfc/rsrvs.cfc?wsdl&method=spomecs&space_query_id=211911&start_dt=2011-10-23&end_dt=2011-10-23
		$.ajax({
			url: uri,
			method: "GET",
			dataType: "json",
			error: function() {
				//do nothing?
			},
			success: function(rsrvs_response) {
				rsrv_data = [];
				try {
					rsrv_data = rsrvs_response["reservations"]["reservation"];
				} catch (jsonException) {
					//there was no reservation data returned
					rsrv_data = [];
				}
				DataSet.reset(); //clear out previously held data
				$(routerThis._dataset_view).remove();
				//routerThis._data_set.reset();
				if (_.isArray(rsrv_data)) {
					$(rsrv_data).each(function(idx){
						this.id = this.reservation_id._text;
						routerThis._data_set.add(this);
					});
				} else {
					//not an array, just a single reservation
					routerThis._data_set.add(rsrv_data);
				}
				routerThis._dataset_view.destroy();
				routerThis._dataset_view = new DataSetView({ collection : routerThis._data_set } );
				routerThis._dataset_view.render();
			},
			complete: function() {
				//cleanup
				//render dataset
				//remove previously existing dataset view
					//disable spinner, hide modal dialog
				routerThis._setup_view.hide();
				$(".activity").spin(false);
                      $("#content").show(800);
				routerThis._dataset_view.render();
			}
		});
	},
	reset: function() {
		console.log("route: reset");
		//this._request_model.destroy();
		if (!this._dataset_view) {
			console.log("RESET: CREATE DATASETVIEW");
			//this._dataset_view = new DataSetView({el: $("#content"), collection: this._data_set, model: this._report_params});
		}
		console.log("APP: Index");
		app.navigate("", true);
	}
});