spomecs
=======

Operational report of event setup needs pulled with R25WS, takes formatting queues from the daily operations report.

Goals are:
- Provide a near real-time alternative to the daily ops report
- Enable the creation of todo workflow items for each event
- Edit space and resource instructions (maybe)
- Enable HTML links to event custom attributes (images, diagrams, files)
- Quick edit of basic event details (contacts, resource quantities, etc) (maybe, hard)

Requirements:
- Resource25 WebServices v2.6 or later

Installation:
- Use the included ant build file to create a .war deployable web application (ant dist)
- Deploy the web archive in the same servlet container (tomcat, glassfish, etc) as your R25WS instance
- Default context root is /spomecs/
- Setup any IIS/Apache proxy forwarding if your using those to front your http requests with the new context root
